FROM richarvey/nginx-php-fpm:latest

LABEL description "PostfixAdmin is a web based interface used to manage mailboxes" \
   application="Postfix Admin"

ARG POSTFIXADMIN_VERSION=3.2.2
ARG POSTFIXADMIN_SHA512=6c84cb215e69c52c26db0651e5d0d9d8bcb0a63b00d3c197f10fa1f0442a1fde44bb514fb476a1e68a21741d603febac67282961d01270e5969ee13d145121ee

RUN apk add \
   dovecot

RUN docker-php-ext-install imap

RUN set -eu; \
   curl -fsSL -o postfixadmin.tar.gz "https://github.com/postfixadmin/postfixadmin/archive/postfixadmin-${POSTFIXADMIN_VERSION}.tar.gz"; \
   echo "$POSTFIXADMIN_SHA512 *postfixadmin.tar.gz" | sha512sum -c -; \
   # upstream tarball include ./postfixadmin-postfixadmin-${POSTFIXADMIN_VERSION}/
   tar -xf postfixadmin.tar.gz -C /var/www/html --strip-components=1; \
   rm postfixadmin.tar.gz; \
   # Does not exist in tarball but is required
   mkdir -p /var/www/html/templates_c; \
   chown -R www-data:www-data /var/www/html

WORKDIR /var/www/html 

COPY rootfs /
RUN chmod +x /usr/local/bin/* /start.sh

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
   && ln -sf /dev/stderr /var/log/nginx/error.log

LABEL maintainer="Marvin Roman <marvinroman@protonmail.com>"
LABEL version="0.0.7"

EXPOSE 443 80
CMD ["/start.sh"]
